﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MusicTickets.Data.Migrations
{
    public partial class ChangeTicketToList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Ticket_TicketId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_Event_Genre_GenreId",
                table: "Event");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_TicketId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "TicketId",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "name",
                table: "Genre",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "GenreId",
                table: "Event",
                newName: "EventGenreId");

            migrationBuilder.RenameIndex(
                name: "IX_Event_GenreId",
                table: "Event",
                newName: "IX_Event_EventGenreId");

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "Ticket",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_ApplicationUserId",
                table: "Ticket",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Event_Genre_EventGenreId",
                table: "Event",
                column: "EventGenreId",
                principalTable: "Genre",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ticket_AspNetUsers_ApplicationUserId",
                table: "Ticket",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Event_Genre_EventGenreId",
                table: "Event");

            migrationBuilder.DropForeignKey(
                name: "FK_Ticket_AspNetUsers_ApplicationUserId",
                table: "Ticket");

            migrationBuilder.DropIndex(
                name: "IX_Ticket_ApplicationUserId",
                table: "Ticket");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "Ticket");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Genre",
                newName: "name");

            migrationBuilder.RenameColumn(
                name: "EventGenreId",
                table: "Event",
                newName: "GenreId");

            migrationBuilder.RenameIndex(
                name: "IX_Event_EventGenreId",
                table: "Event",
                newName: "IX_Event_GenreId");

            migrationBuilder.AddColumn<string>(
                name: "TicketId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_TicketId",
                table: "AspNetUsers",
                column: "TicketId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Ticket_TicketId",
                table: "AspNetUsers",
                column: "TicketId",
                principalTable: "Ticket",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Event_Genre_GenreId",
                table: "Event",
                column: "GenreId",
                principalTable: "Genre",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
