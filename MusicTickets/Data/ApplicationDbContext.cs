﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MusicTickets.Models;

namespace MusicTickets.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<MusicTickets.Models.ApplicationUser> ApplicationUser { get; set; }
        public DbSet<MusicTickets.Models.Event> Event { get; set; }
        public DbSet<MusicTickets.Models.Genre> Genre { get; set; }
        public DbSet<MusicTickets.Models.Stadium> Stadium { get; set; }
        public DbSet<MusicTickets.Models.Ticket> Ticket { get; set; }
    }
}
