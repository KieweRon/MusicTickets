﻿// Write your JavaScript code.
function getCities() {
    var data = {
        resource_id: '894ea7de-08d0-45ce-98f2-379977499618'// the resource id
    };
    $.ajax({
        url: 'https://restcountries.eu/rest/v2/all?fields=capital',
        data: data,
        dataType: 'json',
        success: function (response) {    // response contains json object in it
            console.log(response);
            //var data = JSON.parse(response.result.records);
            var data = response.filter((currCity) => {
                return currCity.capital;
            }).map((currCity) => {
                return currCity.capital;
            });

            data = data.sort((a, b) => a.localeCompare(b.Name));
            var options;
            for (var i = 0; i < data.length; i++) {
                options += "<option value='" + data[i] + "'>" + data[i] + "</option>";
            }

            $("#city").html(options);    // It will put the dynamic <option> set into the dropdown
        }
    });
}