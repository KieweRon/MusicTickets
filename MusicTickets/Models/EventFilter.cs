﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicTickets.Models
{
    public class EventFilter
    {
        public string StadiumId { get; set; }
        public string GenreId { get; set; }
        public DateTime FromDT { get; set; }
        public DateTime ToDT { get; set; }
        public string ArtistName { get; set; }
    }
}
