﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MusicTickets.Models
{
    public class Ticket
    {
        public string Id { get; set; }

        [Required]
        public int Amount { get; set; }

        public virtual Event Event { get; set; }
    }
}
