﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MusicTickets.Models
{
    public class StatisticsContainer
    {
        public virtual ICollection<SelectListItem> EventsByMonths { get; set; }
        public virtual ICollection<SelectListItem> EventsByCities { get; set; }
        public virtual ICollection<SelectListItem> EventsByStadiums { get; set; }
        public virtual ICollection<SelectListItem> EventsByGenres { get; set; }
    }
}
