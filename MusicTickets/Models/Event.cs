﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MusicTickets.Models
{
    public class Event
    {
        public string Id { get; set; }
        [Required]
        public DateTime EventDate { get; set; }
        [Required]
        public string ArtistName { get; set; }
        [Required]
        public int TotalTicketsNumber { get; set; }
        [Required]
        public int AvailableTicketsNumber { get; set; }

        public virtual Genre EventGenre { get; set; }
        public virtual Stadium Stadium { get; set; }
    }
}
