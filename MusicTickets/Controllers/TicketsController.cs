﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MusicTickets.Data;
using MusicTickets.Models;
using Microsoft.AspNetCore.Authorization;

namespace MusicTickets.Controllers
{
    public class TicketsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public TicketsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Tickets
        public async Task<IActionResult> Index()
        {
            ViewBag.Event = _context.Event.Select(@event => new SelectListItem
            {
                Value = @event.Id,
                Text = @event.ArtistName
            });
            ApplicationUser applicationUser = await _userManager.GetUserAsync(HttpContext.User);
            applicationUser = await _context.ApplicationUser.Where(user => user.Id == applicationUser.Id).Include(user => user.Tickets).
                ThenInclude(ticket => ticket.Event).SingleOrDefaultAsync();

            return View(applicationUser.Tickets);
        }

        // GET: Tickets
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> AllOrders()
        {
            ViewBag.Event = _context.Event.Select(@event => new SelectListItem
            {
                Value = @event.Id,
                Text = @event.ArtistName
            });
            return View(await _context.ApplicationUser
                .Include(user => user.Tickets).
                ThenInclude(ticket => ticket.Event)
                .ToListAsync());
        }

        // GET: Tickets/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _context.Ticket
                .Include(@innerEvent => @innerEvent.Event)
                .Include(@innerEvent => @innerEvent.Event.EventGenre)
                .Include(@innerEvent => @innerEvent.Event.Stadium)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _context.Ticket
                .Include(@innerEvent => @innerEvent.Event)
                .Include(@innerEvent => @innerEvent.Event.EventGenre)
                .Include(@innerEvent => @innerEvent.Event.Stadium)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var ticket = await _context.Ticket.Include(@innerEvent => @innerEvent.Event).SingleOrDefaultAsync(m => m.Id == id);
            _context.Ticket.Remove(ticket);
            ticket.Event.AvailableTicketsNumber += ticket.Amount;
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TicketExists(string id)
        {
            return _context.Ticket.Any(e => e.Id == id);
        }
    }
}
