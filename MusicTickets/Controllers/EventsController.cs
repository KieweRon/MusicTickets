﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MusicTickets.Models;
using MusicTickets.Data;
using System.Diagnostics;
using Accord.MachineLearning.Rules;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using System.Security.Claims;

namespace MusicTickets.Controllers
{
    public class EventsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EventsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Events
        public async Task<IActionResult> Index()
        {
            var musicTicketsContext = _context.Event.Include(@innerEvent => @innerEvent.EventGenre).Include(@innerEvent => @innerEvent.Stadium);

            ViewData["EventGenres"] = new SelectList(_context.Set<Genre>(), "Id", "Name");
            ViewData["Stadiums"] = new SelectList(_context.Set<Stadium>(), "Id", "Name");

            return View(await musicTicketsContext.ToListAsync());
        }

        // GET: Events/Details/5
        public async Task<IActionResult> Details(string id)
        {
            ViewData["EventGenres"] = new SelectList(_context.Set<Genre>(), "Id", "Name");
            ViewData["Stadiums"] = new SelectList(_context.Set<Stadium>(), "Id", "Name");
            
            if (id == null)
            {
                return NotFound();
            }

            var @event = await _context.Event
                .Include(@innerEvent => @innerEvent.EventGenre)
                .Include(@innerEvent => @innerEvent.Stadium)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (@event == null)
            {
                return NotFound();
            }

            return View(@event);
        }

        // GET: Events/Create
        [Authorize(Roles = "ADMIN")]
        public IActionResult Create()
        {
            ViewData["EventGenres"] = new SelectList(_context.Set<Genre>(), "Id", "Name");
            ViewData["Stadiums"] = new SelectList(_context.Set<Stadium>(), "Id", "Name");

            return View();
        }

        // POST: Events/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Create([Bind("Id,EventDate,ArtistName,TotalTicketsNumber,AvailableTicketsNumber")] Event @event)
        {
            if (ModelState.IsValid)
            {
                @event.Stadium = _context.Stadium.Where(stadium => stadium.Id.Equals(Request.Form["Stadium"].ToString())).First();
                @event.EventGenre = _context.Genre.Where(genere => genere.Id.Equals(Request.Form["EventGenre"].ToString())).First();

                _context.Add(@event);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["EventGenres"] = new SelectList(_context.Set<Genre>(), "Id", "Name");
            ViewData["Stadiums"] = new SelectList(_context.Set<Stadium>(), "Id", "Name");

            return View(@event);
        }

        [HttpGet]
        //public List<Event> Filter(string StadiumId, string GenreId, DateTime FromDT, DateTime ToDT, string ArtistName)
        public List<Event> Filter(string StadiumId, string GenreId, DateTime FromDT, DateTime ToDT, string ArtistName)
        {
            var musicTicketsContext = _context.Event.Include(@innerEvent => @innerEvent.EventGenre).
                                                     Include(@innerEvent => @innerEvent.Stadium);

            return musicTicketsContext.Where(e => (e.Stadium.Id == StadiumId || StadiumId == null) &&
                                      (e.EventGenre.Id == GenreId || GenreId == null) &&
                                      (e.EventDate >= FromDT || FromDT == DateTime.MinValue) &&
                                      (e.EventDate <= ToDT || ToDT == DateTime.MinValue) &&
                                      (e.ArtistName.Contains(ArtistName) || ArtistName == null)).ToList();
        }

        [HttpGet]
        public Boolean IsAdmin()
        {
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            return roles.Any(currRole => currRole.Value.Equals("ADMIN"));
        }

        // GET: Events/Order/5
        public async Task<IActionResult> Order(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @event = await _context.Event
                .SingleOrDefaultAsync(m => m.Id == id);
            if (@event == null)
            {
                return NotFound();
            }

            ViewData["Event"] = @event;

            ViewData["RecomendedTicket"] = await this.GetTicketsForMI(id);

            return View();
        }

        // POST: Events/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Order([Bind("Amount")] int Amount, string id)
        {
            if (ModelState.IsValid)
            {
                var eventSelected = _context.Event.SingleOrDefault(x => x.Id == id);
                if (eventSelected != null)
                {
                    var ticket = new Ticket() { Amount = Amount, Event = eventSelected };
                    _context.Ticket.Add(ticket);
                    eventSelected.AvailableTicketsNumber -= ticket.Amount;
                    var currUser = _context.ApplicationUser.SingleOrDefault(
                            x => x.Email.Equals(this.User.Identity.Name));
                    if (currUser.Tickets == null)
                    {
                        currUser.Tickets = new List<Ticket>();
                    }
                    currUser.Tickets.Add(ticket);
                    _context.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            return View();
        }

        // GET: Events/Edit/5
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @event = await _context.Event.SingleOrDefaultAsync(m => m.Id == id);
            if (@event == null)
            {
                return NotFound();
            }

            ViewData["EventGenres"] = new SelectList(_context.Set<Genre>(), "Id", "Name");
            ViewData["Stadiums"] = new SelectList(_context.Set<Stadium>(), "Id", "Name");

            return View(@event);
        }

        // POST: Events/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Edit(string id, [Bind("Id,EventDate,ArtistName,TotalTicketsNumber,AvailableTicketsNumber")] Event @event)
        {
            if (id != @event.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    @event.Stadium = _context.Stadium.Where(stadium => stadium.Id.Equals(Request.Form["Stadium"].ToString())).First();
                    @event.EventGenre = _context.Genre.Where(genere => genere.Id.Equals(Request.Form["EventGenre"].ToString())).First();

                    _context.Update(@event);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EventExists(@event.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            ViewData["EventGenres"] = new SelectList(_context.Set<Genre>(), "Id", "Name");
            ViewData["Stadiums"] = new SelectList(_context.Set<Stadium>(), "Id", "Name");

            return View(@event);
        }

        // GET: Events/Delete/5
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            // TODO: get the text
            var @event = await _context.Event
                .Include(@innerEvent => @innerEvent.EventGenre)
                .Include(@innerEvent => @innerEvent.Stadium)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (@event == null)
            {
                return NotFound();
            }

            return View(@event);
        }

        // POST: Events/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var @event = await _context.Event.SingleOrDefaultAsync(m => m.Id == id);
            _context.Event.Remove(@event);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw new ApplicationException("You cant delete this event!") ;
            }
            return RedirectToAction(nameof(Index));
        }

        private bool EventExists(string id)
        {
            return _context.Event.Any(e => e.Id == id);
        }
        public IActionResult Error()
        {
            //return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
            var feature = this.HttpContext.Features.Get<IExceptionHandlerFeature>();
            return View("~/Views/Shared/Error.cshtml", feature?.Error);
        }

        private async Task<Event> GetTicketsForMI(string ticketId)
        {
            Event result = null;
            var userTicketContexts = _context.ApplicationUser.Include(@innterUser => @innterUser.Tickets);
            var ticketEventsContexts = _context.Ticket.Include(@innterTicket => @innterTicket.Event);

            ApplicationUser[] allUsers = await userTicketContexts.ToArrayAsync();
            Ticket[] allTickets = await ticketEventsContexts.ToArrayAsync();

            List<string[]> tempDataset = new List<string[]>();

            // We need data set in form of [["1"], ["1","2"]]
            // For example:
            // string[][] dataset =
            //{
            //    new string[] { "1", "2", "5" },
            //    new string[] { "2", "4" },
            //    new string[] { "2", "3" },
            //    new string[] { "1", "2", "4" },
            //    new string[] { "1", "3" },
            //    new string[] { "2", "3" },
            //    new string[] { "1", "3" },
            //    new string[] { "1", "2", "3", "5" },
            //    new string[] { "1", "2", "3" },
            //};
            for (int userIndex = 0; userIndex < allUsers.Length; userIndex++)
            {
                string[] orderTickets = allUsers[userIndex].Tickets.ToArray().Select(currTicket => currTicket.Id).ToArray();
                
                if (orderTickets.Length > 0)
                {
                    List<string> eventsIds = new List<string>();

                    foreach (string currTicketId in orderTickets)
                    {
                        eventsIds.Add(allTickets.Where(innerTicket => innerTicket.Id == currTicketId).Select(secondTiket => secondTiket.Event.Id).First());
                    }

                    tempDataset.Add(eventsIds.ToArray());
                }
            }

            string[][] dataset = tempDataset.ToArray();


            // For this example, we will consider a database consisting of 9 transactions:
            // - Minimum support count required will be 2 (i.e. min_sup = 2 / 9 = 22%);
            // - Minimum confidence required should be 70%;

            // The dataset of transactions can be as follows:

            // Create a new A-priori learning algorithm with the requirements
            var apriori = new Apriori<string>(threshold: 2, confidence: 0.7);

            // Use apriori to generate a n-itemset generation frequent pattern
            AssociationRuleMatcher<string> classifier = apriori.Learn(dataset);

            // Generate association rules from the itemsets:
            AssociationRule<string>[] rules = classifier.Rules;
            
            // The result should be:
            // {
            //   [5]   -> [1];   support: 2, confidence: 1,
            //   [5]   -> [2];   support: 2, confidence: 1,
            //   [4]   -> [2];   support: 2, confidence: 1,
            //   [5]   -> [1 2]; support: 2, confidence: 1
            // }

            string recomendedTicketId = "";
            

            // Getting the first event from the suggested
            foreach (AssociationRule<string> currRule in rules)
            {
                foreach (var currItemInRule in currRule.X)
                {
                    if (currItemInRule.Equals(ticketId))
                    {
                        recomendedTicketId = currRule.Y.First();

                        break;
                    }
                }

                if (recomendedTicketId.Length > 0)
                {
                    break;
                }
            }

            if (recomendedTicketId.Length > 0)
            {
                result = _context.Event.Where(currEvent => currEvent.Id == recomendedTicketId).First();
            }

            return result;
        }
    }
}
