﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MusicTickets.Data;
using MusicTickets.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

namespace MusicTickets.Controllers
{
    [Authorize(Roles = "ADMIN")]
    public class StatisticsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly string[] monthNames = new string[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

        public StatisticsController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<StatisticsContainer> Data()
        {
            var musicTicketsContext = _context.Event;
            var stadiumContext = _context.Stadium;
            var genreContext = _context.Genre;

            StatisticsContainer statisticsContainer = new StatisticsContainer();

            statisticsContainer.EventsByMonths = await musicTicketsContext.GroupBy(@event => new
            {
                @event.EventDate.Month
            }).Select(@event => new SelectListItem
            {
                Text = monthNames[@event.Key.Month - 1],
                Value = string.Format("{0}", @event.Count())
            }).ToListAsync();

            var ticketsWithStatiumJoinQuery = musicTicketsContext.Join(
                stadiumContext,
                @event => @event.Stadium.Id,
                @stadium => @stadium.Id,
                (@event, @stadium) => new { @event, @stadium }
                );

            statisticsContainer.EventsByCities = await ticketsWithStatiumJoinQuery
                .GroupBy(@eventWithStadium => new
                {
                    @eventWithStadium.@stadium.City
                }).Select(@eventWithStadium => new SelectListItem
                {
                    Text = string.Format("{0}", @eventWithStadium.Key.City),
                    Value = string.Format("{0}", @eventWithStadium.Count())
                }).ToListAsync();

            statisticsContainer.EventsByStadiums = await ticketsWithStatiumJoinQuery
                .GroupBy(@eventWithStadium => new
                {
                    @eventWithStadium.@stadium.Name
                }).Select(@eventWithStadium => new SelectListItem
                {
                    Text = string.Format("{0}", @eventWithStadium.Key.Name),
                    Value = string.Format("{0}", @eventWithStadium.Count())
                }).ToListAsync();

            statisticsContainer.EventsByGenres = await musicTicketsContext.Join(
                genreContext,
                @event => @event.EventGenre.Id,
                @genre => @genre.Id,
                (@event, @genre) => new { @event, @genre }
                )
                .GroupBy(@eventWithStadium => new
                {
                    @eventWithStadium.@genre.Name
                }).Select(@eventWithStadium => new SelectListItem
                {
                    Text = string.Format("{0}", @eventWithStadium.Key.Name),
                    Value = string.Format("{0}", @eventWithStadium.Count())
                }).ToListAsync();

            return statisticsContainer;
        }
    }
}